# Real time video analysis

The goal of this mini-project was to implement a movement detection using OpenCV and laptop camera.
Current result:

![Gif](https://i.imgur.com/omnUl46.gif)

The initial thought was to use hand movement to switch between Windows 10 desktops. This is still under consideration, but postponed at the moment.