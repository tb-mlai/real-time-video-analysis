import cv2
import numpy as np
import collections

K_LAST_POINTS = 5
gaussian_kernel = (1, 1)
morph_kernel = np.ones((3, 3), np.uint8)

videoCapture = cv2.VideoCapture(0)
pixelDifference = None
centerOfMassImage = None
centerOfMassImageAverage = None
rawPixelDifference = None

blackImage = None

kLastCenterPoints = collections.deque([])


def extract_movement(previous_frame, current_frame):
    previous_template = cv2.GaussianBlur(previous_frame, gaussian_kernel, 0)
    current_template = cv2.GaussianBlur(current_frame, gaussian_kernel, 0)

    raw_pixel_difference = previous_template - current_template

    pixel_difference = cv2.morphologyEx(raw_pixel_difference, cv2.MORPH_OPEN, morph_kernel, iterations=4)
    _, pixel_difference = cv2.threshold(pixel_difference, 10, 255, cv2.THRESH_BINARY)
    return raw_pixel_difference, pixel_difference


def get_center_of_mass(img):
    contours, _ = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    size = len(contours)

    if size == 0:
        return None, None

    center_x, center_y = 0, 0

    for contour in contours:
        moment = cv2.moments(contour)
        center_x += int(moment['m10'] // moment['m00'])
        center_y += int(moment['m01'] // moment['m00'])

    center_x, center_y = center_x//size, center_y//size

    return center_x, center_y


def update_average_center_of_mass(k_points, new_point):
    k_points.appendleft(new_point)

    if len(k_points) < K_LAST_POINTS:
        return None
    else:
        k_points.pop()
        return tuple([sum(x) // K_LAST_POINTS for x in zip(*k_points)])


def draw_circle(img, center):
    cv2.circle(img, center, 7, (0, 0, 255), -1)


def prepare_frames(previous_frame, current_frame):
    raw_pixel_difference, pixel_difference = extract_movement(previous_frame, current_frame)
    center = get_center_of_mass(pixel_difference)

    if not all(center):
        # return None, None, None
        return pixel_difference, None, raw_pixel_difference

    pixel_difference = cv2.cvtColor(pixel_difference, cv2.COLOR_GRAY2RGB)
    draw_circle(pixel_difference, center)

    average_center_point = update_average_center_of_mass(kLastCenterPoints, center)

    if average_center_point is None:
        return pixel_difference, None, raw_pixel_difference

    center_of_mass_image_average = blackImage.copy()
    draw_circle(center_of_mass_image_average, average_center_point)

    return pixel_difference, center_of_mass_image_average, raw_pixel_difference


def read_frame():
    _, frame = videoCapture.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    return gray


currentFrame = None
previousFrame = None

while True:
    newFrame = read_frame()

    if blackImage is None:
        blackImage = np.zeros(cv2.cvtColor(newFrame, cv2.COLOR_GRAY2RGB).shape, np.uint8)

    if np.array_equal(currentFrame, newFrame) is True:
        continue
    else:
        previousFrame = currentFrame
        currentFrame = newFrame

    if previousFrame is not None and currentFrame is not None:
        pixelDifference, centerOfMassImageAverage, rawPixelDifference = prepare_frames(previousFrame, currentFrame)

    if pixelDifference is not None:
        cv2.imshow('difference', pixelDifference)

    if centerOfMassImageAverage is not None:
        cv2.imshow('average center of mass', centerOfMassImageAverage)

    if newFrame is not None:
        cv2.imshow('frame2', newFrame)

    if rawPixelDifference is not None:
        cv2.imshow('raw difference', rawPixelDifference)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break


videoCapture.release()
cv2.destroyAllWindows()
